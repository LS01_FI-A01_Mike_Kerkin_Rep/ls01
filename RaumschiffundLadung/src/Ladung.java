
public class Ladung {
	
// die Attribute sind f�r keinen einsichtbar, deshalb die Kapselung "private" und die Attribute werden deklariert	
private String bezeichnung;
private int anzahl;


public Ladung() {
	
}
// eine Klasse ladung wird generiert
public Ladung(String bezeichnung, int anzahl) {
	super();
	this.bezeichnung = bezeichnung;
	this.anzahl = anzahl;
}

// Bezeichnung der Ladung wird ausgegeben
public String getBezeichnung() {
	return bezeichnung;
}
// die Ladung erh�lt einen Namen
public void setBezeichnung(String bezeichnung) {
	this.bezeichnung = bezeichnung;
}
//Anzahl der Ladungen werden ausgegeben
public int getAnzahl() {
	return anzahl;
}
// die Anzahl der Ladungen werden gesetzt
public void setAnzahl(int anzahl) {
	this.anzahl = anzahl;
}


}
