import java.util.ArrayList;


//Attribute werden deklariert	
public class Raumschiff {
	private String name;
	private int energieversorgungInProzent;
	private int schutzschildInProzent;
	private int lebenserhaltungssystemInProzent;
	private int huelleInProzent;
	private int photonentorpedo;
	private int reperaturAndroid;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis;


	
	
	



// 	Eigenschaften der Klassen werden mit einem Konstruktor vorgegeben 
public Raumschiff(String name, int energieversorgungInProzent, int schutzschildInProzent,
		int lebenserhaltungssystemInProzent, int huelleInProzent, int photonentorpedo, int reperaturAndroid) {
	super();
	this.name = name;
	this.energieversorgungInProzent = energieversorgungInProzent;
	this.schutzschildInProzent = schutzschildInProzent;
	this.lebenserhaltungssystemInProzent = lebenserhaltungssystemInProzent;
	this.huelleInProzent = huelleInProzent;
	this.photonentorpedo = photonentorpedo;
	this.reperaturAndroid = reperaturAndroid;

}


//Name wird aus der Klasse entnommen
public String getName() {
	return name;
	

}




//Name wird �bergeben
public void setName(String name) {
	this.name = name;
	

}




// die EnergieversorgungInProzent wird aus der Klasse entnommen
public int getEnergieversorgungInProzent() {
	return energieversorgungInProzent;
}




// Energieversorgung wird gesetzt
public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
	this.energieversorgungInProzent = energieversorgungInProzent;
}



// die Schutzschilde in Prozent werden aus der Klasse Entnommen
public int getSchutzschildInProzent() {
	return schutzschildInProzent;
}




// Schutzschilde werden in der Klasse gesetzt
public void setSchutzschildInProzent(int schutzschildInProzent) {
	this.schutzschildInProzent = schutzschildInProzent;
}




// Lebenserhaltungssysteme werden in Prozent aus der Klasse entnommen
public int getLebenserhaltungssystemInProzent() {
	return lebenserhaltungssystemInProzent;
}




// Lebenserhaltungssysteme werden in der Klasse deklariert
public void setLebenserhaltungssystemInProzent(int lebenserhaltungssystemInProzent) {
	this.lebenserhaltungssystemInProzent = lebenserhaltungssystemInProzent;
}




// Huelle des Raumschiffes wird aus der Klasse entnommen
public int getHuelleInProzent() {
	return huelleInProzent;
}



// Huelle wird in der Klasse deklariert
public void setHuelleInProzent(int huelleInProzent) {
	this.huelleInProzent = huelleInProzent;
}




// Anzahl der Torpedos werden aus der Klasse entnommen
public int getPhotonentorpedo() {
	return photonentorpedo;
}




//Torpedos werden in der Klasse deklariert 
public void setPhotonentorpedo(int photonentorpedo) {
	this.photonentorpedo = photonentorpedo;
}




// Anzahl der Androiden werden ausgegeben 
public int getReperaturAndroid() {
	return reperaturAndroid;
}




// Anzahl der Androiden werden gesetzt 
public void setReperaturAndroid(int reperaturAndroid) {
	this.reperaturAndroid = reperaturAndroid;
}







public void schiessePhotonentorpedos(Raumschiff raumschiff) {
	// TODO
}

public void schiessePhaserkanonen(Raumschiff raumschiff) {
	// TODO
}



private void treffer(Raumschiff raumschiff) {
	
	System.out.println(raumschiff.getName() + " wurde getroffen!");
	
	
	raumschiff.setHuelleInProzent(raumschiff.getHuelleInProzent()-50);
	
	if(raumschiff.getHuelleInProzent() <= 0) {
	raumschiff.setHuelleInProzent(raumschiff.getHuelleInProzent()-50);
		
	raumschiff.setEnergieversorgungInProzent(raumschiff.getEnergieversorgungInProzent()-50);
	if(raumschiff.getHuelleInProzent() <= 0) {
	   	raumschiff.setLebenserhaltungssystemInProzent(0);
	   		sendeNarchichtAnAlle("Lebenserhaltungssysteme sind vernichtet worden");
		}
	}
}


public void sendeNarchichtAnAlle(String string) {
	
System.out.println("nachricht");

		 ArrayList<String> Nachricht = new ArrayList<String>();
		 Nachricht.add("nachricht");
		
this.setBroadcastKommunikator(Nachricht);

}

// eine neue ArrayList wird erstellt mit dem Namen Ladung und diese wird als "ladung" dem Raumschiff hinzugef�gt
public void addLadungen(Ladung ladung) {
	//eine neue ArrayList wird mit den Datentyp "Ladung" generiert
	ArrayList<Ladung> lad = new ArrayList<Ladung>();
	
	lad.add(ladung);
	//wird dem Raumschiff hinzugef�gt mit .addLadung
	this.setLadungsverzeichnis(lad);
}

public ArrayList<String> logbuchAusgeben() {
	return this.getBroadcastKommunikator();
}

public void repariereSchildeHuelleEnergieversorgungVonAndroiden(boolean schutzschild, boolean energieversorgung, boolean schiffshuelle, int anzahlDerAndroiden) {
	// TODO
}

public void zustandRaumschiffAusgeben() {
	// TODO
	System.out.println("getName:-" + this.getName() + "\n" + "getEnergieversorgungInProzent:" + this.getEnergieversorgungInProzent() + "\n" + "getEnergieversorgungInProzent:" + this.getHuelleInProzent() +
				 "\n" + "getLebenserhaltungssystemInProzent:" + this.getLebenserhaltungssystemInProzent() + "\n" + "getSchutzschildInProzent:" + this.getSchutzschildInProzent() + "\n" + "getPhotonentorpedo:" + this.getPhotonentorpedo() + "\n" + "getReperaturAndroid:" + this.getReperaturAndroid() + "\n");
	//Die Getter der jeweiligen Gr��en werden mit entsprechenden Namen ausgegeben
}

public void ladungsVerzeichnisAusgeben() {
for(Ladung ausgabe: this.getLadungsverzeichnis()) {
       
    	 System.out.println("Bezeichnung: " + ausgabe.getBezeichnung() + "\nAnzahl: " + ausgabe.getAnzahl());
    }
      
         System.out.println();
}
public void ladungsVerzeichnisAufraeumen(Ladung ladung) {
	// TODO
}





/**
 * @return the broadcastKommunikator
 */
public ArrayList<String> getBroadcastKommunikator() {
	return broadcastKommunikator;
}





/**
 * @param broadcastKommunikator the broadcastKommunikator to set
 */
public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
	this.broadcastKommunikator = broadcastKommunikator;
}





/**
 * @return the ladungsverzeichnis
 */
public ArrayList<Ladung> getLadungsverzeichnis() {
	return ladungsverzeichnis;
}




	
/**
 * @param ladungsverzeichnis the ladungsverzeichnis to set
 */
public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
	this.ladungsverzeichnis = ladungsverzeichnis;
}





public void setLadungverzeichnis(Ladung ladungKlingonen1, Ladung ladungKlingonen2) {
	// TODO Auto-generated method stub
	
}









}

