package Angestellter;

public class TestAngestellter {

	public static void main(String[] args) {
		  
		//Erzeugen der Objekte
		Angestellter ang1 = new Angestellter( "Olaf", 4500, "Kr�ger");
		Angestellter ang2 = new Angestellter( "Hans", 6000, "Wurst");
	    /* TODO: 7. Erzeugen Sie ein zusaetzliches Objekt ang3 und geben Sie es auch auf der Konsole aus, 
	     * die Attributwerte denken Sie sich aus.
	     */  
		Angestellter ang3 = new Angestellter("Karl", 10000, "Heinz");
	      
	      
	    /*TODO: 8. Erzeugen Sie zwei zusaetzliche Objekte ang4 und ang5
	     * mit dem Konstruktor, der den Namen und Vornamen initialisiert,
	     * die Attributwerte denken Sie sich aus.
	     */
		Angestellter ang4 = new Angestellter( "Komischer", "Robert");
	    Angestellter ang5 = new Angestellter( "Toller", "Frederic");
	    
	    //Setzen der Attribute
	    /* TODO: 9. Fuegen Sie ang4 und ang5 jeweils ein Gehalt hinzu, 
	     * die Attributwerte denken Sie sich aus.
	     * Geben Sie ang4 und ang5 auch auf dem Bildschirm aus.
	     */
		ang4.setGehalt(1600);
		ang4.setName("George");
	 	ang4.setVorname("Robert");

		ang5.setGehalt(2000);
		ang5.setName("Reibetanz");
		ang5.setVorname("Frederic");
    	//Bildschirmausgabe
		System.out.println("Angestellter 1 \n");
		System.out.println("Name: " + ang1.getName());
		System.out.println("Vorname: " + ang1.getVorname());
		System.out.println("Gehalt: " + ang1.getGehalt() + " $");
		System.out.println("Vollname: " + ang1.vollname());
		System.out.println("___________________________________");
		
		System.out.println("Angestellter 2");
    	System.out.println("\nName: " + ang2.getName());
		System.out.println("Vorname: " + ang2.getVorname());
		System.out.println("Gehalt: " + ang2.getGehalt() + " $");
		System.out.println("Vollname: " + ang2.vollname());
		System.out.println("___________________________________");
		
		System.out.println("Angestellter 3 \n");
		System.out.println("Name: " + ang3.getName());
		System.out.println("Vorname: " + ang3.getVorname());
		System.out.println("Gehalt: " + ang3.getGehalt() + " $");
		System.out.println("Vollname: " + ang3.vollname());
		System.out.println("___________________________________");
		
		System.out.println("Angestellter 4 \n");
		System.out.println("Name: " + ang4.getName());
		System.out.println("Vorname: " + ang4.getVorname());
		System.out.println("Gehalt: " + ang4.getGehalt() + " $");
		System.out.println("Vollname: " + ang4.vollname());
		System.out.println("___________________________________");
		
		System.out.println("Angestellter 5 \n");
		System.out.println("Name: " + ang5.getName());
		System.out.println("Vorname: " + ang5.getVorname());
		System.out.println("Gehalt: " + ang5.getGehalt() + " $");
		System.out.println("Vollname: " + ang5.vollname());
		
	}

}
