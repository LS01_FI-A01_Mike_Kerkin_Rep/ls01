package Angestellter;

public class Angestellter {

	 private String name;
	 private double gehalt;
	 private String vorname;
	 //TODO: 3. Fuegen Sie in der Klasse 'Angestellter' das Attribut 'vorname' hinzu und implementieren Sie die entsprechenden get- und set-Methoden.
	 //TODO: 4. Implementieren Sie einen Konstruktor, der alle Attribute initialisiert.
	 //TODO: 5. Implementieren Sie einen Konstruktor, der den Namen und den Vornamen initialisiert.
	 public Angestellter() {
		 
	 }
	 
	 public Angestellter(String name, double gehalt, String vorname) {
		this.name = name;
		this.gehalt = gehalt;
		this.vorname = vorname;
			
	}

	 public Angestellter(String name, String vorname) {
		this.name = name;
		this.vorname = vorname;
		
	}

	 
	 
	public String getVorname() {
		return vorname;
	}
	public void setVorname(String vorname) {
		this.vorname = vorname;
			
	}

	
	
	public String getName() {
	    return name;
	 }
	 public void setName(String name) {
		    this.name = name;
		   
	 }


	 public double getGehalt() {
		return gehalt;
	}
	public void setGehalt(double gehalt) {
	   //TODO: 1. Implementieren Sie die entsprechende set-Methoden. 
	   //Ber�cksichtigen Sie, dass das Gehalt nicht negativ sein darf.
		 if (gehalt > 0) {
			 this.gehalt = gehalt;
		}		 
	 }
 
	
	 
	 //TODO: 6. Implementieren Sie eine Methode 'vollname', die den vollen Namen (Vor- und Zuname) als string zur�ckgibt.
	public String vollname() {
		return this.getVorname() + " " + this.getName();
	}
}
